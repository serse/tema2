function addTokens(input, tokens){
    if (typeof input === 'string' || input instanceof String)
    {
        if(input.length>=6)
        {
            for(let i = 0; i <tokens.length;i++)
            {
                if(typeof tokens[i]['tokenName'] === 'string' || tokens[i]['tokenName'] instanceof String)
                {
                    var n = input.indexOf("...");
                    if(n===-1)
                    {
                        return input;
                    }
                    else
                    {
                        let nume = "${" + tokens[i]['tokenName'].toString() + "}";
                        input = input.replace("...",`${nume}`);
                        return input;
                    }
                    
                }
                else
                {
                    let err_tkn_format = new Error("Invalid array format");
                    throw err_tkn_format;
                }
            }
                    
        }
        else
        {
            let err_inp_length = new Error("Input should have at least 6 characters");
            throw err_inp_length;
        }
    }
    else
    {
        let err_inp_type = new Error("Invalid input");
		throw err_inp_type;
    }
}

const app = {
    addTokens: addTokens
}

module.exports = app;